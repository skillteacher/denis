using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class SmartEnemy : MonoBehaviour
{
    #region ColiderCheck's
    [SerializeField] private ColliderCheck checkGround;
    [SerializeField] private ColliderCheck checkAbyss;
    [SerializeField] private ColliderCheck checkJump;
    [SerializeField] private ColliderCheck checkWall;
    [SerializeField] private ColliderCheck checkPlatform;

    #endregion
    
    [SerializeField] private Transform targer;
    [SerializeField] private Transform spriteTransform;
    [SerializeField] private string groundLayerName = "Ground";
    [SerializeField] private float speed = 11f;
    [SerializeField] private float jumpForce = 12f;
    [SerializeField] private float chaseRange = 15f;
    private Rigidbody2D enemyRigidbody2D;
    private bool IsAbyssAhead;

    private void Awake()
    {
        enemyRigidbody2D = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        if (Vector2.Distance(transform.position,targer.position) <= chaseRange)
        {
            ChaseTarget();
        }
    }

    private void ChaseTarget()
    {
        float direction = transform.position.x < targer.position.x ? 1 : -1;
        spriteTransform.localScale = new Vector2(direction, 1f);
        Vector2 velocity = new Vector2(speed * direction, enemyRigidbody2D.velocity.y);
        enemyRigidbody2D.velocity = velocity;
    }
    private void Jump()
    {
        Vector2 jumpImpulse = new Vector2(enemyRigidbody2D.velocity.x, jumpForce);
        enemyRigidbody2D.velocity = jumpImpulse;
    }


  








}



