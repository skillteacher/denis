using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    //[SerializeField] private int lives = 3;
    //public int GetLives { get => lives;}
    private Game game;

    private Vector3 startPosition;

    private void Awake()
    {
        game = FindObjectOfType<Game>();
        //game.SetPlayerHealth(this);
        if (game == null) Debug.Log("� �� ����� ������ Game");
    }
    
    private void Start()
    {
        startPosition = transform.position;
    }
    public void TakeDamage()
    {
        //lives--;
        game.LoseLife();
        transform.position = startPosition;
        //PlayerUI.ui.SetLives(lives);
    }
}
