using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Krokodil : MonoBehaviour
{
    [SerializeField] private string plaerLayerMame = "Vrag";
    [SerializeField] private float speed = 10f;
    private bool isTurnedRight;
    private Rigidbody2D vragRigidbody;
    private int groundLayerNumder;

    private void Awake()
    {
        vragRigidbody = GetComponent<Rigidbody2D>();
        groundLayerNumder = LayerMask.NameToLayer(plaerLayerMame);
    }

    private void Update()
    {
        Vector2 velocity;
        if (isTurnedRight)
        {
            velocity = new Vector2(speed, vragRigidbody.velocity.y);

        }
        else
        {
            velocity = new Vector2(-speed, vragRigidbody.velocity.y);
        }
        vragRigidbody.velocity = velocity;

    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (groundLayerNumder != collision.gameObject.layer) return;

        Vector3 scale = transform.localScale;
        transform.localScale = new Vector3(-scale.x, scale.y, scale.z);        
    }
}
