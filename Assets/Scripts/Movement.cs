using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] private Collider2D feetCollider;
    private float speed = 10f;
    [SerializeField] private float jumpForce = 10f;
    private KeyCode jumpCode = KeyCode.Space;
    private Rigidbody2D playerRigidbody;
    //private Animator playerAnimator;
    private bool spriteFlip;
    private bool isGrounded;

    private void Awake()
    {
        playerRigidbody = GetComponent<Rigidbody2D>();
        //playerAnimator = GetComponent<Animator>();
    }

    private void Update()
    {
        float horizontalInput = Input.GetAxis("Horizontal"); 
        Move(horizontalInput);
        Flip(horizontalInput);
        isGrounded = feetCollider.IsTouchingLayers(LayerMask.GetMask("Ground"));
        if (Input.GetKeyDown(jumpCode) && isGrounded) Jump();        
    }

    private void Move(float direction)
    {
        //if (direction != 0) playerAnimator.SetBool("Run", true);
        //else playerAnimator.SetBool("Run", false);
        Vector2 velocity = new Vector2(speed * direction, playerRigidbody.velocity.y);
        playerRigidbody.velocity = velocity;
    }

    private void Jump()
    {
        Vector2 jumpVector = new Vector2(0f, jumpForce);
        playerRigidbody.velocity += jumpVector;
    }

    private void Flip(float direction)
    {
        bool needFlip = (direction > 0 && spriteFlip) || (direction < 0 && !spriteFlip);
        Vector3 scale = transform.localScale;
        if (needFlip)
        {
            spriteFlip = !spriteFlip;
            transform.localScale = new Vector3(-1f * scale.x, scale.y, scale.z);
        }
    }
}
