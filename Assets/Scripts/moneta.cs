using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moneta : MonoBehaviour
{
    [SerializeField] private int coinCost = 1;
    [SerializeField] private string playerLayer = "Player";
    private bool isCollected;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer != LayerMask.NameToLayer(playerLayer)) return;

        PickCoin();

    }

    private void PickCoin()
    {
        if (isCollected) return;
        isCollected = true;
        FindObjectOfType<GameManager>().AddCoin(coinCost);
        Destroy(gameObject);
    }












}