using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class opacnoct : MonoBehaviour
{
    [SerializeField] private float delay = 1f;
    private bool alreadyHit;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var health = collision.GetComponent<Health>();
        if (!health && alreadyHit) return;
        health.TakeDamage();
        alreadyHit = true;
        StartCoroutine(Wait(delay));
    }

    private IEnumerator Wait(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        alreadyHit = false;
    }





}
