using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField] private int startLifeAmount = 3;
    [SerializeField] private string firstLevelName;
    [SerializeField] private Text finalCoinCount;
    [SerializeField] private GameObject menu;
    private int coinCount;
    private int livesCount;


    private void Awake()
    {
        GameManager[] gameManager = FindObjectsOfType<GameManager>();
        if (gameManager.Length > 1)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Start()
    {
        livesCount = startLifeAmount;
    }

    public void RestartGame()
    {
        coinCount = 0;
        livesCount = startLifeAmount;
        Time.timeScale = 1f;
        menu.SetActive(false);
        SceneManager.LoadScene(firstLevelName);
        PlayerUI.ui.SetLives(livesCount);
    }

    public void AddCoin(int amount)
    {
        coinCount += amount;
        PlayerUI.ui.ShowCoinCount(coinCount);
    }

    public void LoseLife()
    {
        livesCount--;
        PlayerUI.ui.SetLives(livesCount);
        if (livesCount <= 0) GameOver();
    }

    private void GameOver()
    {
        Time.timeScale = 0f;
        menu.SetActive(true);
        finalCoinCount.text = coinCount.ToString();
    }
    public void ExitGame()
    {
        Application.Quit();
    }




}
